/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author RNagel
 */
public class S4_Suggestions_And_Innovations_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S4_Suggestions_And_Innovations_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.MTN;

        //*******************************************
    }

    //S4_Suggestions and Innovations_Beta
    @Test
    public void S4_Suggestions_and_Innovations_Beta() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\S4_Suggestions and Innovations_Beta.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    
    
    //FR1-Capturing Suggestions And Innovation - Main Scenario
    @Test
    public void FR1_Capturing_Suggestions_And_Innovation_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capturing_Suggestions_And_Innovation - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capturing_Suggestions_And_Innovation_Optional_ScenarioScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capturing_Suggestions_And_Innovation - Optional_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capturing_Suggestions_And_Innovation_Alternative_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capturing_Suggestions_And_Innovation - AlternateScenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2_Capture_Suggestions_and_Innovations_Actions
    @Test
    public void FR2_Capture_Suggestions_and_Innovations_Actions() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2_Capture_Suggestions_and_Innovations_Actions.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
}
