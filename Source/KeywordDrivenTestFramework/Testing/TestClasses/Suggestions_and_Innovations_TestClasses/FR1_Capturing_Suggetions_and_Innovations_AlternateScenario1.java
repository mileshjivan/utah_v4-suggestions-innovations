/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Suggestions_and_Innovations_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Suggestions_and_Innovations_PageObjects.SuggestionsAndInnovations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Suggestions and Innovation Alt1",
        createNewBrowserInstance = false
)

public class FR1_Capturing_Suggetions_and_Innovations_AlternateScenario1 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capturing_Suggetions_and_Innovations_AlternateScenario1()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if(!changeDetails()){
            return narrator.testFailed("Change Details Failed due to - " + error);
        }    
        return narrator.finalizeTest("Completed navigate to Suggestions And Innovations");
    }
    public boolean changeDetails(){
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus())){
            error = "Failed to wait for Suggestion Status dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus())){
            error = "Failed to click Suggestion Status dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus_Select(testData.getData("Suggestion Status")))){
            error = "Failed to wait for '"+testData.getData("Suggestion Status")+"' in Suggestion Status dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus_Select(testData.getData("Suggestion Status")))){
            error = "Failed to select '"+testData.getData("Suggestion Status")+"' in Suggestion Status dropdown.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Suggestion Status changed.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Save_Button())){
            error = "Failed to wait for Save button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_Save_Button())){
            error = "Failed to click Save button.";
            return false;
        }
        
       //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(SuggestionsAndInnovations_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
         //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.inspection_RecordSaved_popup())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(SuggestionsAndInnovations_PageObjects.inspection_RecordSaved_popup());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(SuggestionsAndInnovations_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        return true;
    }

}
