/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Suggestions_and_Innovations_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
//import KeywordDrivenTestFramework.Testing.PageObjects.Risk_Register_PageObjects.RiskRegister_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Suggestions_and_Innovations_PageObjects.SuggestionsAndInnovations_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Suggestions_and_Innovations_PageObjects.SuggestionsAndInnovations_PageObjects_2;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.Keys;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Suggestions and Innovation Alt3",
        createNewBrowserInstance = false
)

public class FR1_Capturing_Suggestions_And_Innovation_AlternateScenario3 extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public FR1_Capturing_Suggestions_And_Innovation_AlternateScenario3() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
//        if (!IsometrixNavigateSuggestionsAndInnovations()) {
//            return narrator.testFailed("Failed due - " + error);
//        }

        if (!enterDetails()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed navigate to Suggestions And Innovations - Alternate Scenario 3.");
    }

//    public boolean IsometrixNavigateSuggestionsAndInnovations()
//    {
//        //Navigate to Environmental Health & Safety
//        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.navigate_EHS())) {
//            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.navigate_EHS())) {
//            error = "Failed to click on 'Environmental, Health & Safety' tab.";
//            return false;
//        }
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
//
//        //Navigate to Risk Management
//        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.suggestions_and_innovations())) {
//            error = "Failed to wait for 'Suggestions And Innovations.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.suggestions_and_innovations())) {
//            error = "Failed to click on 'Suggestions And Innovations' tab.";
//            return false;
//        }
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Suggestions And Innovations' tab.");
//
//        //Add button
//        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Add())) {
//            error = "Failed to wait for 'Add' button.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_Add())) {
//            error = "Failed to click on 'Add' button.";
//            return false;
//        }
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
//
//        return true;
//    }

    public boolean enterDetails() {
        pause(5000);
        
        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_suggestionStatus_DropDown())) {
            error = "Failed to wait for 'Suggestion Status' dropdown button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.SaI_suggestionStatus_DropDown())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_suggestionStatus_DropDown())) {
                error = "Failed to wait for 'Suggestion Status' dropdown button.";
                return false;
            }
            pause(5000);
            if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.SaI_suggestionStatus_DropDown())) {
                error = "Failed to click 'Suggestion Status' dropdown button.";
                return false;
            }

        }

        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_suggestionStatus(testData.getData("Suggestion Status")))) {
            error = "Failed to wait for '" + testData.getData("Status") + "' Suggestion Status option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.SaI_suggestionStatus(testData.getData("Suggestion Status")))) {
            error = "Failed to click '" + testData.getData("Status") + "' Suggestion Status option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_RejectedBy_DropDown())) {
            error = "Failed to wait for 'Rejected By' dropdown button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.SaI_RejectedBy_DropDown())) {
            error = "Failed to click 'RejectedBy' dropdown button.";
            return false;
        }
        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_RejectedBy(testData.getData("RejectedBy")))) {
            error = "Failed to wait for '" + testData.getData("RejectedBy") + "' Suggestion Status option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.SaI_RejectedBy(testData.getData("RejectedBy")))) {
            error = "Failed to click '" + testData.getData("RejectedBy") + "' Rejected By option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_dateRejected_textxpath())) {
            error = "Failed to wait for 'Date Rejected' text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_dateRejected_textxpath(), date)) {
            error = "Failed to wait for 'Date Rejected' text field.";
            return false;
        }

        SeleniumDriverInstance.pressKey(Keys.ENTER);

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_comments_textxpath())) {
            error = "Failed to wait for 'Comments' text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_comments_textxpath(), testData.getData("Comments"))) {
            error = "Failed to wait for 'Comments' text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.SaI_Save_button())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.SaI_Save_button())) {
            error = "Failed to click 'Save' button.";
            return false;
        }

        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(SuggestionsAndInnovations_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
         //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.inspection_RecordSaved_popup())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(SuggestionsAndInnovations_PageObjects.inspection_RecordSaved_popup());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(SuggestionsAndInnovations_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

        return true;
    }

}
