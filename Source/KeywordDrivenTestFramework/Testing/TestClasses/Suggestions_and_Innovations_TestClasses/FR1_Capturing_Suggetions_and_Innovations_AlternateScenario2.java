/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Suggestions_and_Innovations_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
//import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Suggestions_and_Innovations_PageObjects.SuggestionsAndInnovations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Suggestions and Innovation Alt2",
        createNewBrowserInstance = false
)

public class FR1_Capturing_Suggetions_and_Innovations_AlternateScenario2 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capturing_Suggetions_and_Innovations_AlternateScenario2()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if(!changeDetails()){
            return narrator.testFailed("Change Details Failed due to - " + error);
        }    
        
        return narrator.finalizeTest("Completed navigate to Suggestions And Innovations");
    }

    public boolean IsometrixNavigateSuggestionsAndInnovations(){
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(SuggestionsAndInnovations_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Suggestions And Innovations
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.suggestions_and_innovations())){
            error = "Failed to wait for 'Suggestions And Innovations.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.suggestions_and_innovations())){
            error = "Failed to click on 'Suggestions And Innovations' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Suggestions And Innovations' tab.");
       
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    public boolean changeDetails(){
        //Suggestion Status
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus())){
            error = "Failed to wait for 'Suggection Status' dropdown to be present.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus())){
            error = "Failed to wait for 'Suggestion Status' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus())){
            error = "Failed to click on 'Suggestion Status' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus_Select(getData("Suggestion Status")))){
            error = "Failed to wait for '" + getData("Suggestion Status") + "' in Suggestion Status dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus_Select(getData("Suggestion Status")))){
            error = "Failed to select '" + getData("Suggestion Status") + "' in Suggestion Status dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggestion Status changed to '" + getData("Suggestion Status") + "' .");
        
        //Suggestion priority
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.suggestionPriorityTab())){
            error = "Failed to wait for 'Suggestion priority' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.suggestionPriorityTab())){
            error = "Failed to click on 'Suggestion priority' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.suggestionPriority(getData("Suggestion Priority")))){
            error = "Failed to wait for '" + getData("Suggestion Priority") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.suggestionPriority(getData("Suggestion Priority")))){
            error = "Failed to click on '" + getData("Suggestion Priority") + "' option.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Suggestion priority '" + getData("Suggestion Priority") + "' has been selected.");
        
        //Approved by
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.approvedByTab())){
            error = "Failed to wait for 'Approved by' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.approvedByTab())){
            error = "Failed to wait for 'Approved by' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.approvedBy(getData("Approved by")))){
            error = "Failed to wait for '" + getData("Approved by") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.approvedBy(getData("Approved by")))){
            error = "Failed to click on '" + getData("Approved by") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Approved by '" + getData("Approved by") + "' has been selected.");
        
        //Date Approved
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.dateApprovedTab())){
             error = "Failed to wait for 'Date approved' tab";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.dateApproved(), getData("Date approved"))){
             error = "Failed to enter '" + getData("Date approved") + "' into 'Date approved' field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Date approved '" + getData("Aproved by") + "' has been selected.");
        
        //Comments textarea
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.commentsTextarea())){
            error = "Failed to wait for 'Comments' textarea";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.commentsTextareaField(), getData("Comments"))){
            error = "Failed to enter '" + getData("Comments") + "' into 'Comments' textarea";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully commented '" + getData("Comments") + "'.");
        
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Save_Button())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_Save_Button())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(SuggestionsAndInnovations_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
         //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.inspection_RecordSaved_popup())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(SuggestionsAndInnovations_PageObjects.inspection_RecordSaved_popup());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(SuggestionsAndInnovations_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        
        return true;
    }

}
